<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Teste Builders</title>
</head>
<body>
	<form action="/builders/clientes" method="POST">
		<div>
			<label>Nome</label>
			<input type="text" name="nome" />
			
		</div>
		<div>
			<label>CPF</label>
			<input type="text" name="cpf" />
		</div>
		<div>
			<label>Data de Nascimento</label>
			<input type="text" name="dataDeNascimento" />
		</div>
		<button type="submit">Cadastrar</button>
	</form>
</body>
</html>