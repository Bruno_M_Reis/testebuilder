<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulário teste builders</title>
</head>
<body>



	<form:form action="${s:mvcUrl('CC#atualizar').build()}" method="PUT">
		Nome:<input type="text" name="nome" value="${cliente.nome}">
		CPF:<input type="text" name="nome" value="${cliente.cpf}"> 
		Data De Nascimento:<fmt:formatDate pattern="dd/MM/yyy" value="${cliente.dataDeNascimento.time }"/>
		<input type="text" name="id" value="${cliente.id}" hidden="true">
		<input type="submit">
	</form:form>
</body>
</html>